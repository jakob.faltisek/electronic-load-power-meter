/*
 * uart0.h
 *
 * Created: 10.05.2016 18:24:16
 *  Author: Jakob
 */ 


#ifndef UART0_H_
#define UART0_H_

#include <stdio.h>

 //definition of uart callback function pointer
typedef	void(*uart_func)(uint8_t);

/*
 * Function to initializes the uart1, you need to call it to use to functions below.
 * to enable the transmitting part, write a '1' to the send argument
 * to enable the transmitting via interrupts, write a '1' to the sendInterrupt argument
 * to enable the receiving part, write a '1' to the receive argument.
 * to enable the interrupt part of the receive function, write a '1' to the receiveInterrupt argument. 
 * to configure uart0 as the stdout of the mC (e.g. to use the printf function) write a '1' to the configureAsStdout argument.
 */ 
void uart0_init(uint16_t bautRate, uint8_t send, uint8_t sendInterrupt, uint8_t receive, uint8_t receiveInterrupt, uint8_t configureAsStdout);
/*
 * Function to register a Callback function, which will be executed in the ISR 
 * of the UART0 when it receives an byte.
 */
void uart0_registerCallbackFunction(uart_func callback);


/*
 * Main Functions for the transmitting part. The data to send will be stored in a queue
 * and automatically via interrupts transmitted. 
 */ 
void uart0_putc(char c);
void uart0_puts(char* pS);
void uart0_newLine();
void uart0_putsln(char* pS);

/*
 * This function clears all the data in the transmitt queue
 */
 void uart0_clearTransmittQueue();


/*
 * Additional functions for the transmitting part to send integers. The data will also be 
 * stored an automatically transmitted. 
 */ 
void uart0_print_uint32 (uint32_t printValue);
void uart0_print_uint16 (uint16_t printValue);
void uart0_print_uint8 (uint8_t printValue);
void uart0_print_int32 (int32_t printValue);
void uart0_print_int16 (int16_t printValue);
void uart0_print_int8 (int8_t printValue);


/*
* Function if you are using the receive parte without intterrupt.
*
* The function uart0_getc(..) checks if a data
* byte has been received completely. In this case we
* return 1 and deliver the received byte.
* If reception is not complete we return 0.
*
* Note: This function MUST be called within the while(1) loop.
*/
uint8_t uart0_getc(char* pData);


#endif /* UART0_H_ */