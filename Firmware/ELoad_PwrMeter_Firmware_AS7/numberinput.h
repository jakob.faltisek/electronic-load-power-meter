/*
 * numberinput.h
 *
 * Created: 05.02.2019 19:56:01
 *  Author: Jakob Faltisek Faltisek
 */ 


#ifndef NUMBERINPUT_H_
#define NUMBERINPUT_H_

#include "general.h"

//Type-Definition a Encoder Rotation
typedef enum DigitControl {DO_NOTHING, INC_DIGIT, DEC_DIGIT, SEL_NEXT_DIGIT} _DIGITCONTROL_TMP_;

enum DigitControl eventsToDigitCtrl(volatile enum _events* pTheEvent);
void numberInput(enum DigitControl digitCtrl, uint16_t min, uint16_t max, uint16_t* pValue, uint8_t* pCurPos);


#endif /* NUMBERINPUT_H_ */