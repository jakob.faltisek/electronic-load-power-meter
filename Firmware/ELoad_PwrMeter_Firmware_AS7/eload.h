/*
 * eload.h
 *
 * Created: 21.01.2019 20:44:18
 *  Author: Jakob Faltisek
 */ 


#ifndef ELOAD_H_
#define ELOAD_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include "general.h"
#include "numberinput.h"

void eload_runCC(enum DigitControl digitCtrl);

#endif /* ELOAD_H_ */