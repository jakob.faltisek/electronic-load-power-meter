/*
 * ads1115.h
 *
 * Created: 17.11.2018 14:28:01
 *  Author: Jakob Faltisek
 */ 


#ifndef ADS1115_H_
#define ADS1115_H_

//Definitions for the ADS1115
#define ADS1115_ADR			0x48 //I2C Address of the 16bit-ADC

//used to set the Address Pointer Register
enum ADS1115_REG
{
	REG_CONV	= 0x00,
	REG_CONFIG	= 0x01
};

//used to set the MUX bits in the Config-Register to select the channel that is measured
enum ADS1115_MUX
{
	SENSE_A0 = 	4,
	SENSE_A1 =	5,
	SENSE_A2 =  6,
	SENSE_A3 =	7
};

//used to set the full scale resolution (FSR) of the programmable gain amplifier (PGA)
enum ADS115_PGA
{
	FSR_6V144 = 0, //FSR = 6.144V
	FSR_4V096 = 1, //...
	FSR_2V048 = 2,
	FSR_1V024 = 3,
	FSR_0V512 = 4,
	FSR_0V256 = 5
};

uint16_t ads1115_readRegister(enum ADS1115_REG reg);
void ads1115_writeRegister(enum ADS1115_REG reg, uint16_t data);
void ads1115_startConv(enum ADS1115_MUX mux, enum ADS115_PGA pga);
uint8_t ads1115_readConv(uint16_t* pData);

#endif /* ADS1115_H_ */