/*
 * ads1115.c
 *
 * Created: 13.11.2018 20:02:48
 *  Author: Jakob Faltisek
 */ 

#include <avr/io.h>
#include "general.h"
#include "i2c.h"
#include "uart0.h"

#include "ads1115.h"

void ads1115_startConv(enum ADS1115_MUX mux, enum ADS115_PGA pga)
{
	//start a single conversation, with a data rate of 475 SPS, the channel and the FSR is controlled
	//via the parameters
	ads1115_writeRegister(REG_CONFIG, (0x81C3 | (mux << 12) | (pga << 9)) ); 
}

uint8_t ads1115_readConv(uint16_t* pData)
{
	if ((ads1115_readRegister(REG_CONFIG) & (1<<15)) == 0)
	{
		//the conversation is still running
		return 0;
	}
	else
	{
		//return the conversation value and indicate that the measurement was successful
		*pData = ads1115_readRegister(REG_CONV);
		return 1;
	}
}

void ads1115_writeRegister(enum ADS1115_REG reg, uint16_t data)
{
	//Send start condition, write the address pointer register, write two bytes and send stop condition
	i2c_start(ADS1115_ADR, I2C_WRITE);
	i2c_write((uint8_t)reg);
	i2c_write(data>>8);
	i2c_write(data);
	i2c_stop();
}

uint16_t ads1115_readRegister(enum ADS1115_REG reg)
{
	uint8_t	regHighByte, regLowByte;
	
	//Send start condition, write the Address Pointer Register and send stop condition
	i2c_start(ADS1115_ADR, I2C_WRITE);
	i2c_write((uint8_t)reg);
	i2c_stop();
	
	//Send start condition, read two bytes of the register and send stop condition
	i2c_start(ADS1115_ADR, I2C_READ);
	i2c_readAck(&regHighByte);
	i2c_readAck(&regLowByte);
	i2c_stop();
	
	//printf("Databyte0: 0x%X, Databyte1: 0x%X\n", registerVal[0], registerVal[1]);
	return ((uint16_t)regHighByte<<8) | regLowByte;
}