// ----------------------------------------------------------------------------
/// @file         i2c.h
/// @addtogroup   I2C_LIB   I2C Library (libi2c.a i2c.h)
/// @{
/// @brief        The i2c library provides a set of functions and definitions to
///               communicate with an i2c slave
/// @author       Dietmar Scheiblhofer
// ----------------------------------------------------------------------------

#ifndef _I2C_H
#define _I2C_H   1

#include <avr/io.h>
#include <inttypes.h>
#include <compat/twi.h>

#ifndef F_CPU
#define F_CPU 16000000L     ///< defines the system's frequency in MHz
#endif

// ----------------------------------------------------------------------------
/// @brief			  used to indicate the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus
{
	I2C_OK											= 0,
	I2C_ERROR_START_TIMEOUT			= 2,
	I2C_ERROR_START_STATUS			= 3,
	I2C_ERROR_ADDRESS_TIMEOUT		= 4,
	I2C_ERROR_ADDRESS_STATUS		= 5,
	I2C_ERROR_STOP_TIMEOUT  		= 6,
	I2C_ERROR_WRITE_TIMEOUT  		= 7,
	I2C_ERROR_WRITE_STATUS  		= 8,
	I2C_ERROR_READACK_TIMEOUT		= 9,
	I2C_ERROR_READNAK_TIMEOUT   = 10
};
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
/// @brief			  used to set the direction of the communication
// ----------------------------------------------------------------------------
enum I2CDirection
{
	I2C_WRITE				= 0,    ///< write data to the i2c slave
	I2C_READ				= 1	    ///< read data from the i2c slave
};
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
#define I2C_TIMER_DELAY 0xFF			///< defines the delay, after which a called i2c function shall be aborted, in case the i2c status hasn't changed properly
// ----------------------------------------------------------------------------

#ifdef __cplusplus
  extern "C" {
#endif
  
// ----------------------------------------------------------------------------
/// @fn           void i2c_init(uint32_t sclClock);
/// @brief        Initializes the i2c module; sets the communication clock
/// @param[in]    sclClock		the i2c clock speed; e.g. 100000L or 400000L
// ----------------------------------------------------------------------------
void i2c_init(uint32_t sclClock);

// ----------------------------------------------------------------------------
/// @fn           enum I2CStatus i2c_start(uint8_t i2cAddress, enum I2CDirection direction);
/// @brief        Sends a start condition to the i2c slave
/// @param[in]    i2cAddress		the address of the slave to be addressed
/// @param[in]    direction		  the direction of the communication
/// @return       the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus i2c_start(uint8_t i2cAddress, enum I2CDirection direction);

// ----------------------------------------------------------------------------
/// @fn           enum I2CStatus i2c_stop(void);
/// @brief        Sends a stop condition to the i2c slave
/// @return       the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus i2c_stop(void);

// ----------------------------------------------------------------------------
/// @fn           enum I2CStatus i2c_write(uint8_t data);
/// @brief        Write a data byte to the slave; the direction of the communication
///               must have been set to I2C_WRITE (see i2c_start)
/// @param[in]    data      the data to be written
/// @return       the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus i2c_write(uint8_t data);

// ----------------------------------------------------------------------------
/// @fn           enum I2CStatus i2c_readAck(uint8_t *data);
/// @brief        Reads a byte from the slave and acknowledges the reception; the
///								direction of the communication must have been set to I2C_READ
/// @param[out]   data        the data read from the slave
/// @return       the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus i2c_readAck(uint8_t *data);

// ----------------------------------------------------------------------------
/// @fn           enum I2CStatus i2c_readNak(uint8_t *data);
/// @brief        Reads a byte from the slave but does not acknowledge the 
///               reception. This indicates the slave that the communication is
///               over an no further bytes will be read. The communication must 
/// 							have been set to I2C_READ
/// @param[out]   data        the data read from the slave
/// @return       the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus i2c_readNak(uint8_t *data);

// ----------------------------------------------------------------------------
/// @fn           enum I2CStatus i2c_readBytes(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *data, uint8_t noBytes);
/// @brief        Reads one or more bytes from an i2c slave. i2c_readBytes firstly
///               addresses the i2c slave by @ref i2c_start, then writes the register
///               address to the i2c slave, and afterwards reads a number of
/// 							bytes. The communication is ended by the stop condition.
/// @param[in]    i2cAddress        the i2c address of the slave
///	@param[in]    registerAddress   the address of the register to read from
/// @param[out]   data              the data read from the slave	
/// @param[in]    noBytes           the number of bytes to be read
/// @return       the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus i2c_readBytes(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *data, uint8_t noBytes);

// ----------------------------------------------------------------------------
/// @fn           enum I2CStatus i2c_writeBytes(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *data, uint8_t noBytes);
/// @brief        Writes one or more bytes to an i2c slave. i2c_writeBytes firstly
/// 							addresses the i2c slave by @ref i2c_start, then writes the register 
///								address to the slave, followed by a number of bytes and the
///								stop condition.
/// @param[in]    i2cAddress        the i2c address of the slave
/// @param[in]    registerAddress   the address of the register to write to
/// @param[in]    data              the data to be written to the slave
/// @param[in]    noBytes           the number of bytes to be written
/// @return       the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus i2c_writeBytes(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *data, uint8_t noBytes);

// ----------------------------------------------------------------------------
/// @fn           enum I2CStatus i2c_readByte(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *data);
/// @brief        Reads a single byte from an i2c slave. i2c_readByte firstly
///               addresses the i2c slave by @ref i2c_start, then writes the register
///               address to the i2c slave, and afterwards reads one byte.
/// 							The communication is ended by the stop condition.
/// @param[in]    i2cAddress        the i2c address of the slave
///	@param[in]    registerAddress   the address of the register to read from
/// @param[out]   data              the byte read from the slave
/// @return       the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus i2c_readByte(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *data);

// ----------------------------------------------------------------------------
/// @fn           enum I2CStatus i2c_writeByte(uint8_t i2cAddress, uint8_t registerAddress, uint8_t data);
/// @brief        Writes a single byte to an i2c slave. i2c_writeByte firstly
/// 							addresses the i2c slave by @ref i2c_start, then writes the register
///								address to the slave, followed by a byte and the stop condition.
/// @param[in]    i2cAddress        the i2c address of the slave
/// @param[in]    registerAddress   the address of the register to write to
/// @param[in]    data              the byte to be written to the slave
/// @return       the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus i2c_writeByte(uint8_t i2cAddress, uint8_t registerAddress, uint8_t data);

// ----------------------------------------------------------------------------
/// @fn           enum I2CStatus i2c_readBit(uint8_t i2cAddress, uint8_t registerAddress, uint8_t bitNum, uint8_t *data);
/// @brief        Reads a single bit from an i2c slave. i2c_readByte firstly
///               addresses the i2c slave by @ref i2c_start, then writes the register
///               address to the i2c slave, and afterwards reads the bit.
/// 							The communication is ended by the stop condition.
/// @param[in]    i2cAddress        the i2c address of the slave
///	@param[in]    registerAddress   the address of the register to read from
/// @param[in]    bitNum            the number of the bit to be read
/// @param[out]   data              the bit read from the slave
/// @return       the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus i2c_readBit(uint8_t i2cAddress, uint8_t registerAddress, uint8_t bitNum, uint8_t *data);

// ----------------------------------------------------------------------------
/// @fn           enum I2CStatus i2c_writeBit(uint8_t i2cAddress, uint8_t registerAddress, uint8_t bitNum, uint8_t data);
/// @brief        Writes a single bit to an i2c slave. i2c_writeByte firstly
/// 							addresses the i2c slave by @ref i2c_start, then writes the register
///								address to the slave, followed by a single bit and the stop condition.
/// @param[in]    i2cAddress        the i2c address of the slave
/// @param[in]    registerAddress   the address of the register to write to
/// @param[in]    bitNum            the number of the bit to be written
/// @param[in]    data              the bit to be written to the slave
/// @return       the i2c status
// ----------------------------------------------------------------------------
enum I2CStatus i2c_writeBit(uint8_t i2cAddress, uint8_t registerAddress, uint8_t bitNum, uint8_t data);

#ifdef __cplusplus
};
#endif

#endif

/// @}
