/*
 * ssd1306.c
 *
 * Created: 10.11.2018 12:35:54
 * Author : Jakob Faltisek
 *
 * Driver to interface with the SSD1306 128x64 OLED Display (Driver) over I2C and display
 * data on the Display.
 * 
 * The code is based of the project: https://bitbucket.org/tinusaur/ssd1306xled 
 * from http://tinusaur.org.
 */ 

// ============================================================================


#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "ssd1306.h"
#include "font6x8.h"
#include "font8x16.h"

#include "general.h" 
#include "uart0.h"
#include "i2c.h"


const uint8_t ssd1306_init_sequence [] = {	// Initialization Sequence
	0xAE,			// Display OFF (sleep mode)
	0x20, 0b00,		// Set Memory Addressing Mode
					// 00=Horizontal Addressing Mode; 01=Vertical Addressing Mode;
					// 10=Page Addressing Mode (RESET); 11=Invalid
	0xB0,			// Set Page Start Address for Page Addressing Mode, 0-7
	0xC8,			// Set COM Output Scan Direction
	0x00,			// ---set low column address
	0x10,			// ---set high column address
	0x40,			// --set start line address
	0x81, 0x3F,		// Set contrast control register
	0xA1,			// Set Segment Re-map. A0=address mapped; A1=address 127 mapped. 
	0xA6,			// Set display mode. A6=Normal; A7=Inverse
	0xA8, 0x3F,		// Set multiplex ratio(1 to 64)
	0xA4,			// Output RAM to Display
					// 0xA4=Output follows RAM content; 0xA5,Output ignores RAM content
	0xD3, 0x00,		// Set display offset. 00 = no offset
	0xD5,			// --set display clock divide ratio/oscillator frequency 
	0xF0,			// --set divide ratio
	0xD9, 0x22,		// Set pre-charge period
	0xDA, 0x12,		// Set com pins hardware configuration		
	0xDB,			// --set vcomh
	0x20,			// 0x20,0.77xVcc
	0x8D, 0x14,		// Set DC-DC enable
	0xAF			// Display ON in normal mode
	
};

// ----------------------------------------------------------------------------


void ssd1306_send_byte(uint8_t byte)
{
	i2c_write(byte);
}

void ssd1306_send_command_start(void) {
	i2c_start(SSD1306_ADR, I2C_WRITE);
	ssd1306_send_byte(0x00);	// write command
}

void ssd1306_send_command_stop(void) {
	i2c_stop();
}

void ssd1306_send_command(uint8_t command)
{
	ssd1306_send_command_start();
	ssd1306_send_byte(command);
	ssd1306_send_command_stop();
}

void ssd1306_send_data_start(void)
{
	i2c_start(SSD1306_ADR, I2C_WRITE);
	ssd1306_send_byte(0x40);	//write data
}

void ssd1306_send_data_stop(void)
{
	i2c_stop();
}

// ----------------------------------------------------------------------------

void ssd1306_init(void)
{
	for (uint8_t i = 0; i < sizeof (ssd1306_init_sequence); i++) {
		i2c_writeByte(SSD1306_ADR, 0x00, ssd1306_init_sequence[i]);
	}
}

void ssd1306_setpos(uint8_t col, uint8_t page)
{
	ssd1306_send_command_start();
	ssd1306_send_byte(0xb0 + page);
	ssd1306_send_byte(((col & 0xf0) >> 4) | 0x10); // | 0x10
	ssd1306_send_byte((col & 0x0f)); // | 0x01
	ssd1306_send_command_stop();
}

void ssd1306_fill4(uint8_t p1, uint8_t p2, uint8_t p3, uint8_t p4) {
	ssd1306_setpos(0, 0);
	ssd1306_send_data_start();
	for (uint16_t i = 0; i < 128 * 8 / 4; i++) {
		ssd1306_send_byte(p1);
		ssd1306_send_byte(p2);
		ssd1306_send_byte(p3);
		ssd1306_send_byte(p4);
	}
	ssd1306_send_data_stop();
}

void ssd1306_fill2(uint8_t p1, uint8_t p2) {
	ssd1306_fill4(p1, p2, p1, p2);
}

void ssd1306_fill(uint8_t p) {
	ssd1306_fill4(p, p, p, p);
}

void ssd1306_fillscreen(uint8_t fill)
{
	uint8_t m,n;
	for (m = 0; m < 8; m++)
	{
		ssd1306_send_command(0xb0 + m);	// page0 - page1
		ssd1306_send_command(0x00);		// low column start address
		ssd1306_send_command(0x10);		// high column start address
		ssd1306_send_data_start();
		for (n = 0; n < 128; n++)
		{
			ssd1306_send_byte(fill);
		}
		ssd1306_send_data_stop();
	}
}

// ----------------------------------------------------------------------------

void ssd1306_char_font6x8(char c) {
	
	//offset to start at the right place in the array
	c -= 32;
	ssd1306_send_data_start();
	for (uint8_t i = 0; i < 6; i++)
	{
		ssd1306_send_byte(pgm_read_byte(&ssd1306xled_font6x8[c * 6 + i]));
	}
	ssd1306_send_data_stop();
}

void ssd1306_char_font8x16(uint8_t col, uint8_t page, char c)
{
	//if the column and the page is to high
	if (col > 120 || page > 6)
		return; //return and to nothing
	
	//offset to start at the right place in the array
	c -= 32;
	
	//send the part of the upper page
	ssd1306_setpos(col, page);
	ssd1306_send_data_start();
	for (uint8_t i = 0; i < 8; i++)
	{
		ssd1306_send_byte(pgm_read_byte(&ssd1306xled_font8x16[c * 16 + i]));
	}
	ssd1306_send_data_stop();
	
	//send the part of the lower page
	ssd1306_setpos(col, page+1);
	ssd1306_send_data_start();
	for (uint8_t i = 0; i < 8; i++)
	{
		ssd1306_send_byte(pgm_read_byte(&ssd1306xled_font8x16[c * 16 + 8 + i]));
	}
	ssd1306_send_data_stop();
	
}

void ssd1306_puts(uint8_t startingCol, uint8_t startingPage, enum ssd1306Font font, char* pS)
{
	//offset of the digit for the FONT8x16, to keep track of which digit is displayed and the right 
	//column can be send to the function
	uint8_t digitOffset = 0; 
	
	//if the column and the page is to high
	if (startingCol > 127 || startingPage > 7)
		return; //return and to nothing
	
	ssd1306_setpos(startingCol, startingPage);
	
	if (font == FONT6X8)
	{
		//send string
		while (*pS) {
			ssd1306_char_font6x8(*pS++);
		}
	}
	else if (font == FONT8X16)
	{
		//send string
		while (*pS) {
			//when the end of one page is reached, go to the beginning of the next one
			if (startingCol + digitOffset * 8 > 120) {
				startingCol = 0;
				digitOffset = 0;
				startingPage += 2;
			}
			ssd1306_char_font8x16(startingCol + digitOffset * 8, startingPage, *pS++);
			digitOffset++;
		}
	}	
}

void ssd1306_printf(uint8_t startingCol, uint8_t startingPage, enum ssd1306Font font, const char* format, ...)
{
	va_list arg;
	char str[100]; 
	
	va_start (arg, format);
	//write the modified string (variables converted to numbers, ...) in the str string
	vsprintf (str, format, arg); 
	va_end (arg);
// 	uart0_puts(str);
	
	//send the modified string
	ssd1306_puts(startingCol, startingPage, font, str);
}

// ============================================================================