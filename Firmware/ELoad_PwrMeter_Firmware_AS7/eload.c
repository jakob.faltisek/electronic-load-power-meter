/*
 * eload.c
 *
 * Created: 21.01.2019 20:16:37
 *  Author: Jakob Faltisek
 */ 

#include "eload.h"
#include "general.h"
#include "ssd1306.h"
#include "ads1115.h"
#include "numberinput.h"

//Function Prototype
void setCurrent(uint16_t I_mA);

//TODO: Constant Resistance CR and Constant Power CP sub menu

void eload_runCC(enum DigitControl digitCtrl)
{
	static uint16_t setCur_mA = 10;
	static uint8_t  selDigit = 1, selDigit_old = 0xFF;
	uint16_t        measVoltage;
	uint16_t        measCurrent;
	uint8_t         ovfCurrOrVol = 0;

	//Read in the input from the encoder
	numberInput(digitCtrl, MIN_CUR_MA, MAX_CUR_MA, &setCur_mA, &selDigit);
	
	//Set PWM Output
	setCurrent(setCur_mA);

	//Display:
	//Header
	ssd1306_puts(15, 0, FONT6X8, "E-LOAD: CC");
	
	//the Set Value
	ssd1306_printf(15, 3, FONT6X8, "%01d.%03dA", setCur_mA/1000%10, setCur_mA%1000);
	
	
	if (selDigit != selDigit_old)
	{
		//clear prev. cursor:
		ssd1306_puts(15, 4, FONT6X8, "     ");
	}
	selDigit_old = selDigit;
	
	//The Cursor for the selected digit
	if (selDigit == 3)
		ssd1306_puts(15, 4, FONT6X8, "^");
	else
		ssd1306_puts(39-selDigit*6, 4, FONT6X8, "^");
	
	//the Measured Current
	ads1115_startConv(SENSE_A0, FSR_4V096);
	while(!ads1115_readConv(&measCurrent));
	ssd1306_printf(70, 3, FONT6X8, "%.3fA", measCurrent*4.096/0x7FFF);

	//The Measured Voltage
	ads1115_startConv(SENSE_A2, FSR_4V096);
	while(!ads1115_readConv(&measVoltage));
	ssd1306_printf(15, 6, FONT6X8, "%.2fV  ", measVoltage*40.096/0x7FFF);
	// 	if (ADC_getAvgVoltage(2, &measVoltage_V)) {
	// 		measVoltage_V = (measVoltage_V/4.7)*104.7;
	// 		display.print(measVoltage_V);
	// 		display.print(" V");
	// 	}
	// 	else {
	// 		display.print("OVF V");
	// 		ovfCurrOrVol = 1;
	// 	}

	//The Measured Power
	ssd1306_printf(70, 6, FONT6X8, "%.2fW    ", (measCurrent*4.096/0x7FFF)*(measVoltage*40.096/0x7FFF));

	// 	if (!ovfCurrOrVol)
	// 	{
	// 		display.print(measVoltage_V*measCurrent_A);
	// 		display.print(" W");
	// 		} else {
	// 		display.print("OVF W");
	// 	}
}

void setCurrent(uint16_t I_mA)
{
	const uint16_t I_OFFSET_MA = 16;
	 
	if (I_mA == 0 || I_mA < I_OFFSET_MA)
	{
		CURSET_VAL = 0;
	}
	else if (I_mA < MAX_CUR_MA)
	{
		CURSET_VAL = I_mA-I_OFFSET_MA;
	}
}