/*
 * ssd1306.c
 *
 * Created: 10.11.2018 12:35:54
 * Author : Jakob Faltisek
 *
 * Driver to interface with the SSD1306 128x64 OLED Display (Driver) over I2C and display
 * data on the Screen.
 * 
 * The code is based of the project: https://bitbucket.org/tinusaur/ssd1306xled 
 * from http://tinusaur.org.
 */ 

#ifndef SSD1306XLED_H
#define SSD1306XLED_H

//============================================================================

//Enumeration that is used to select the font that should
//be used to display a string
enum ssd1306Font {
		FONT6X8,
		FONT8X16
};

//-----------------------------------------------------------------------------

//definition to clear the screen
#define ssd1306_clear()	ssd1306_fill(0)

//-----------------------------------------------------------------------------

void ssd1306_init(void);
void ssd1306_setpos(uint8_t x, uint8_t y);
void ssd1306_fill(uint8_t p);
void ssd1306_fillscreen(uint8_t fill);
void ssd1306_puts(uint8_t startingCol, uint8_t startingPage, enum ssd1306Font font, char* pS);
void ssd1306_char_font8x16(uint8_t col, uint8_t page, char c);
void ssd1306_printf(uint8_t startingCol, uint8_t startingPage, enum ssd1306Font font, const char* format, ...);


// ============================================================================

#endif
