/*
 * i2cmaster.c
 *
 * Created: 19.10.2013 20:23:14
 *  Author: Didi
 */ 

#include "i2c.h"
#include <util/delay.h>

enum I2CStatus	i2cStatus_			= I2C_OK;

void i2c_init(uint32_t sclClock)
{
	TWCR0  &= ~(1 << TWEN);
	TWSR0 = 0;
	TWBR0 = ((F_CPU/sclClock)-16)/2;
	_delay_ms(50);
}

enum I2CStatus i2c_start(uint8_t i2cAddress, enum I2CDirection direction)
{
	uint32_t  i2c_timer = 0;
	uint8_t   twst;
	uint8_t		address;
	
	address   = (i2cAddress << 1) | (uint8_t)direction;
	
	TWCR0 = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);				// send START condition

	// wait until transmission completed
	i2c_timer = I2C_TIMER_DELAY;
	while(!(TWCR0 & (1<<TWINT)) && i2c_timer--);
	if(i2c_timer == 0)		return (i2cStatus_ = I2C_ERROR_START_TIMEOUT);

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TWSR0 & 0xF8;
	if ( (twst != TW_START) && (twst != TW_REP_START)) 		return (i2cStatus_ = I2C_ERROR_START_STATUS);

	// send device address
	TWDR0 = address;
	TWCR0 = (1<<TWINT) | (1<<TWEN);

	// wail until transmission completed and ACK/NACK has been received
	i2c_timer = I2C_TIMER_DELAY;
	while(!(TWCR0 & (1<<TWINT)) && i2c_timer--);
	if(i2c_timer == 0)		return (i2cStatus_ = I2C_ERROR_ADDRESS_TIMEOUT);

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TWSR0 & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) )
	return (i2cStatus_ = I2C_ERROR_ADDRESS_STATUS);

	return (i2cStatus_ = I2C_OK);
}

enum I2CStatus i2c_stop(void)
{
	uint32_t  i2c_timer = 0;

	/* send stop condition */
	TWCR0 = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
	
	// wait until stop condition is executed and bus released
	i2c_timer = I2C_TIMER_DELAY;
	while((TWCR0 & (1<<TWSTO)) && i2c_timer--);
	if(i2c_timer == 0)		return (i2cStatus_ = I2C_ERROR_STOP_TIMEOUT);

	return (I2C_OK);
}

enum I2CStatus i2c_write(uint8_t data)
{
	uint32_t  i2c_timer = 0;
	uint8_t   twst;
	
	// send data to the previously addressed device
	TWDR0 = data;
	TWCR0 = (1<<TWINT) | (1<<TWEN);

	// wait until transmission completed
	i2c_timer = I2C_TIMER_DELAY;
	while(!(TWCR0 & (1<<TWINT)) && i2c_timer--);
	if(i2c_timer == 0)		return (i2cStatus_ = I2C_ERROR_WRITE_TIMEOUT);

	// check value of TWI Status Register. Mask prescaler bits
	twst = TWSR0 & 0xF8;
	if( twst != TW_MT_DATA_ACK) return (i2cStatus_ = I2C_ERROR_WRITE_STATUS);

	return I2C_OK;
}

enum I2CStatus i2c_readAck(uint8_t *data)
{
	uint32_t  i2c_timer = 0;

	TWCR0 = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);

	i2c_timer = I2C_TIMER_DELAY;
	while(!(TWCR0 & (1<<TWINT)) && i2c_timer--);
	if(i2c_timer == 0)	return (i2cStatus_ = I2C_ERROR_READACK_TIMEOUT);

	*data = TWDR0;
	return I2C_OK;
}/* i2c_readAck */

enum I2CStatus i2c_readNak(uint8_t *data)
{
	uint32_t  i2c_timer = 0;

	TWCR0 = (1<<TWINT) | (1<<TWEN);

	i2c_timer = I2C_TIMER_DELAY;
	while(!(TWCR0 & (1<<TWINT)) && i2c_timer--);
	if(i2c_timer == 0)	return (i2cStatus_ = I2C_ERROR_READNAK_TIMEOUT);
		
	*data = TWDR0;
	return (I2C_OK);
}/* i2c_readNak */

enum I2CStatus i2c_readBytes(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *data, uint8_t noBytes)
{
	if (i2c_start(i2cAddress, I2C_WRITE) != I2C_OK)		return (i2cStatus_);
	if (i2c_write(registerAddress)   != I2C_OK)				return (i2cStatus_);
	_delay_us(10);
	if (i2c_start(i2cAddress, I2C_READ) != I2C_OK)		return (i2cStatus_);
	while (noBytes > 1)
	{
		if (i2c_readAck(data++) != I2C_OK)							return (i2cStatus_);
		noBytes--;
	}
	if (i2c_readNak(data) != I2C_OK)									return (i2cStatus_);
	i2c_stop();
			
	return (i2cStatus_);
}

enum I2CStatus i2c_writeBytes(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *data, uint8_t noBytes)
{
	if (i2c_start(i2cAddress, I2C_WRITE) != I2C_OK)		return (i2cStatus_);
	if (i2c_write(registerAddress)   != I2C_OK)				return (i2cStatus_);
	while (noBytes > 0)
	{
		if (i2c_write((*data)++) != I2C_OK)							return (i2cStatus_);
		noBytes--;
	}
	i2c_stop();
	
	return (i2cStatus_);
}

enum I2CStatus i2c_writeByte(uint8_t i2cAddress, uint8_t registerAddress, uint8_t data) 
{
  return i2c_writeBytes(i2cAddress, registerAddress, &data, 1);
}

enum I2CStatus i2c_readByte(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *data) 
{
  return i2c_readBytes(i2cAddress, registerAddress, data, 1);
}

enum I2CStatus i2c_writeBit(uint8_t i2cAddress, uint8_t registerAddress, uint8_t bitNum, uint8_t data) 
{
  uint8_t b;
  i2c_readByte(i2cAddress, registerAddress, &b);
  b = (data != 0) ? (b | (1 << bitNum)) : (b & ~(1 << bitNum));
  return i2c_writeByte(i2cAddress, registerAddress, b);
}

enum I2CStatus i2c_readBit(uint8_t i2cAddress, uint8_t registerAddress, uint8_t bitNum, uint8_t *data) 
{
  uint8_t b;
  uint8_t count = i2c_readByte(i2cAddress, registerAddress, &b);
  *data = b & (1 << bitNum);
  return count;
}
