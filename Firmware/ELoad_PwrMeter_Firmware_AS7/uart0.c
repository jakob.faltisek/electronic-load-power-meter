/*
 * uart0.c
 *
 * Created: 12.10.2016 08:18:32
 * Author : Jakob
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "uart0.h"
#include "queue.h"

#define		F_CPU		16000000
#include <util/delay.h>

static volatile uint8_t		receiveReady;
static volatile uint8_t		receiveData;
static volatile uint8_t		sendInterruptEnabled;

static volatile uart_func	uartCallbackFunction;

//definitions for the stdio 
void uart0_putc_stream(uint8_t c, FILE *stream);
//uint8_t uart0_getc_stream(FILE *stream);
static FILE uartStream = FDEV_SETUP_STREAM(uart0_putc_stream, NULL, _FDEV_SETUP_WRITE);
//static FILE uartStream_input = FDEV_SETUP_STREAM(NULL,uart0_getc_stream, _FDEV_SETUP_READ);

void uart0_init(uint16_t bautRate, uint8_t send, uint8_t sendInterrupt, uint8_t receive, uint8_t receiveInterrupt, uint8_t configureAsStdout)
{
	//set bautrate
	UBRR0 = F_CPU/16/bautRate - 1;
	
	if (send)
	{
		UCSR0B |= ((1<<TXEN0));
		if (sendInterrupt)
		{
			initTransmittQueue();
			sendInterruptEnabled = 1;
			sei();
		}
	}
	
	if (receive)
	{
		UCSR0B |= (1<<RXEN0);

		if (receiveInterrupt)
		{
			UCSR0B |= (1<<RXCIE0);
			receiveReady = 0;
			sei();
		}
	}

	if (configureAsStdout)
	{
		stdout = &uartStream; // setup our stdout stream
		//stdin = &uartStream_input; // setup our stdin stream
	}
	
	//set character size to 8 bit and no parity
	UCSR0C = (3<<UCSZ00);
}

void uart0_registerCallbackFunction(uart_func callback)
{
	uartCallbackFunction = callback; 
}


/*Main Transmitting part*/

ISR (USART0_UDRE_vect)
{
	if (isEmptyTransmittQueue() )
	{
		//turn off interrupt for the buffer register
		UCSR0B &= ~(1<<UDRIE0);
	}
	else
	{
		UDR0 = getCharFromTransmittQueue();
	}
}

void uart0_putc_int(char c)
{
	//turn off interrupt for the buffer register
	UCSR0B &= ~(1<<UDRIE0);
	
	//add character to the queue
	putCharInTransmittQueue(c);
	
	//turn the interrupt back on
	UCSR0B |= (1<<UDRIE0);
}

void uart0_waitAndPutc(char c)
{
	//wait until UDRE buffer is empty
	while ( !(UCSR0A & (1<<UDRE0)) );
	UDR0 = c;
}

void uart0_putc(char c)
{
	if (sendInterruptEnabled) {
		uart0_putc_int(c);
	} 
	else {
		uart0_waitAndPutc(c);
	}
}

void uart0_puts(char* pS)
{
	if (sendInterruptEnabled)
	{
		//turn off interrupt for the buffer register
		UCSR0B &= ~(1<<UDRIE0);
	
		//add characters to the queue
		while (*pS != '\0')
		{
			putCharInTransmittQueue(*pS);
			++pS;
		}
	
		//turn the interrupt back on
		UCSR0B |= (1<<UDRIE0);
	}
	else {
		while (*pS != '\0')
		{
			uart0_waitAndPutc(*pS);
			++pS;
		}
	}
}


  /*
	The function uart0_getc(..) checks if a data
	byte has been received completely. In this case we 
	return 1 and deliver the received byte. 
	If reception is not complete we return 0.

	Note: This function MUST be called within the while(1) loop.
  */
uint8_t uart0_getc(char* pData)
{
	if ( (UCSR0A & (1<<RXC0)) )
	{
		*pData = UDR0;
		return 1;
	} else {
		return 0;
	}
}

uint8_t uart0_waitAndGetc()
{
	uint8_t data;
	while( !(UCSR0A & (1<<RXC0))); //wait until data are received
	data = UDR0;
	return data; //read the data from receive buffer
}

ISR (USART0_RX_vect)
{
	receiveReady = 1;
	receiveData = UDR0;

	if (uartCallbackFunction)
	(*uartCallbackFunction)(receiveData);
}

//Functions to configure the uart0 as standard input/output 

void uart0_putc_stream(uint8_t c, FILE *stream)
{
	// translate \n to \r
	if (c == '\n')
	uart0_putc('\r');

	uart0_putc(c);
}

// uint8_t uart0_getc_stream(FILE *stream) 
// {
// 	return uart0_waitAndGetc();
// }



/*Other Functions for various types for transmitting*/

void uart0_newLine()
{
	uart0_putc(10); //send carriage return
	uart0_putc(13); //send line feed LF
}

void uart0_putsln(char* pS)
{
	uart0_puts(pS);
	uart0_newLine();
}

//Clear all Data in the Transmitting queue
void uart0_clearTransmittQueue()
{
	removeAllFromTransmittQueue();
}

void uart0_print_uint32 (uint32_t printValue)
{
	uint32_t	tenHighN = 1000000000; //start value 10^9
	
	if (printValue == 0)
	{
		uart0_putc('0');
	}
	else
	{
		for(int8_t n = 9; n >= 0; --n)
		{
			if (printValue >= tenHighN)
			{
				//the digit is no leading zero and has to be displayed
				uart0_putc((uint8_t)((printValue/tenHighN)%10) + '0');
			} else
			{
				//the digit is a leading zero, so to nothing
			}
			tenHighN /= 10;
		}
	}
}

void uart0_print_uint16 (uint16_t printValue)
{
	uint16_t	tenHighN = 10000; //start value 10^4
	
	if (printValue == 0)
	{
		uart0_putc('0');
	}
	else
	{
		for(int8_t n = 4; n >= 0; --n)
		{
			if (printValue >= tenHighN)
			{
				//the digit is no leading zero and has to be displayed
				uart0_putc((uint8_t)((printValue/tenHighN)%10) + '0');
			} else
			{
				//the digit is a leading zero, so to nothing
			}
			tenHighN /= 10;
		}
	}
}

void uart0_print_uint8 (uint8_t printValue)
{
	if (printValue >= 100)
	{
		uart0_putc((printValue/100)%10 + '0');
	}
	if (printValue >= 10)
	{
		uart0_putc(printValue/10 + '0');
	}
	
	uart0_putc(printValue%10 + '0');
}

void uart0_print_int32 (int32_t printValue)
{
	if (printValue >= 0)
	{
		uart0_print_uint32(printValue);
	}
	else
	{
		uart0_putc('-');
		uart0_print_uint32(-printValue);
	}
}

void uart0_print_int16 (int16_t printValue)
{
	if (printValue >= 0)
	{
		uart0_print_uint16(printValue);
	}
	else
	{
		uart0_putc('-');
		uart0_print_uint16(-printValue);
	}
}

void uart0_print_int8 (int8_t printValue)
{
	if (printValue >= 0)
	{
		uart0_print_uint8(printValue);
	}
	else
	{
		uart0_putc('-');
		uart0_print_uint8(-printValue);
	}
}