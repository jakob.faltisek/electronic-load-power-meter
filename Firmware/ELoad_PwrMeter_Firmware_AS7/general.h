/*
 * general.h
 *
 * Created: 10.11.2018 12:30:12
 * Author : Jakob Faltisek
 */ 

#ifndef GENERAL_H_
#define GENERAL_H_

//Type-Definitions for the Events and the States of the State machine that controls the user interface
typedef enum _states {ST_E_LOAD, ST_PWR_METER, ST_BATT} _STATES_TMP_;
typedef enum _events {EV_NO_EVENT, EV_S1, EV_S2, EV_S_ENC, EV_ENC_CW, EV_ENC_CCW} _EVENTS_TMP_;

//Macros for pin operation
#define   setPin(x,y)   ((x) |= (1<<y))
#define   clrPin(x,y)   ((x) &= ~(1<<y))
#define   selPin(x,y)   ((x) & (1<<y))
#define   togPin(x,y)   ((x) ^= (1<<(y)))

//Definitions for the debugging serial signals
#define _DEBUG_EVENTS_
#define _DEBUG_ADC_
#define _DEBUG_STATE_MACHINE_

//Definitions for the Power-On LED
#define DDR_LED_ON  DDRC
#define PORT_LED_ON PORTC
#define LED_ON      PC0

//Definitions for the Encoder Channels and the Encoder Button
#define DDR_ENC     DDRD  
#define PIN_ENC     PIND 
#define PORT_ENC	PORTD
#define ENC_A       PD2 //Encoder Channel A	- INT0
#define ENC_B       PD4 //Encoder Channel B	- PCINT20
#define S_ENC       PD3 //Encoder Button	- INT1

//Definitions for the relays to switch for the A-Range/ E-Load Path
//and the mA- and uA-Range
#define DDR_RELAY  DDRD
#define PORT_RELAY PORTD
#define RELAY      PD5

//Definitions for the MOSFET Switches of the mA- and uA Current Sense Range
#define DDR_CURSEN  DDRD
#define PORT_CURSEN PORTD
#define CURSEN_MA   PD6
#define CURSEN_UA   PD7

//Definitions for the Buttons S1 and S2
#define DDR_BUTTONS		DDRB 
#define PIN_BUTTONS		PINB 
#define PORT_BUTTONS	PORTB
#define S_1				PB0 //PCINT0
#define S_2				PB2 //PCINT2

//Definitions for the ADC Channels to measure the voltage
//of the battery
#define VBAT_P      6 //ADC6 - Battery voltage positive
#define VBAT_N      7 //ADC7 - Battery voltage negative

//Definitions for the PWM Channel that sets the set-voltage of
//Constant Current E-Load
#define DDR_PWM     DDRB
#define PORT_PWM    PORTB
#define PWM_CURSET  PB1
#define CURSET_VAL	OCR1A
#define PWM_TOP		5040 
   

//Definitions for the SSD1308
#define SSD1306_ADR			0x3C //I2C Address of the SSD1308 OLED Display (Driver)


//Definitions of the Electronic Load
#define	MAX_CUR_MA	PWM_TOP//4000 //maximum current in mA
#define	MIN_CUR_MA	10			  //minimum current in mA

#endif