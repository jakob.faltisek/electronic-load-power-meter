/*
 * queue.c
 *
 * Created: 30.03.2016 09:38:12
 *  Author: Jakob
 */ 

#include <avr/io.h>
#include <stdlib.h>

struct QueueNode
{
	char		dat;
	struct QueueNode*	pNext;
};

static struct QueueNode*	pFirst;

void initTransmittQueue()
{
	pFirst = NULL;
}

uint8_t isEmptyTransmittQueue()
{
	if (pFirst == NULL)
	return 1;
	else
	return 0;
}

uint8_t putCharInTransmittQueue(char dat)
{
	struct QueueNode*	pNode;
	struct QueueNode*	pTmp;

	pNode = (struct QueueNode*)malloc(sizeof(struct QueueNode));

	if (pNode != NULL)
	{
		pNode->dat = dat;
		pNode->pNext = NULL;

		if (pFirst == NULL)
		{
			pFirst = pNode;
		}
		else{
			pTmp = pFirst;

			while (pTmp->pNext != NULL)
			{
				pTmp = pTmp->pNext;
			}

			pTmp->pNext = pNode;
		}

		return 1;
	}
	else
	{
		return 0;
	}
}


char getCharFromTransmittQueue(char* pDat)
{
	char		dat;
	struct QueueNode*	pTmp;

	if (pFirst != NULL)
	{
		dat = pFirst->dat;

		pTmp = pFirst->pNext;
		free(pFirst);

		pFirst = pTmp;

		return dat;
	}
	else{
		return 0;
	}
}

void removeAllFromTransmittQueue()
{
	struct QueueNode*	pTmp;

	while (pFirst != NULL)
	{
		pTmp = pFirst->pNext;
		free(pFirst);
		pFirst = pTmp;
	}
}