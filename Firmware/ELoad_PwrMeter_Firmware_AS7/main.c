/*
 * main.c
 *
 * Created: 10.11.2018 12:35:54
 * Author : Jakob Faltisek
 */ 

#define F_CPU 16000000
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "general.h"
#include "uart0.h"
#include "i2c.h"
#include "ads1115.h"
#include "ssd1306.h"
#include "eload.h"
#include "numberinput.h"

//TODO: change Oled Fonts back to originals, because all the vertical pixels work on the new display

volatile enum _states theState;
volatile enum _events theEvent;

//Function Prototypes
uint16_t readBatteryVoltage();
void runELoad();

void init ()
{
	//Initialize the output pins
	DDR_LED_ON |= (1<<LED_ON);
	PORT_LED_ON |= (1<<LED_ON);

	DDR_RELAY |= (1<<RELAY);
	PORT_RELAY &= ~(1<<RELAY);
	//PORT_RELAY |= (1<<RELAY);

	DDR_PWM |= (1<<PWM_CURSET);
	PORT_PWM &= ~(1<<PWM_CURSET);

	//Initialize the input pins
	DDR_BUTTONS &= ~((1<<S_1)|(1<<S_2));
	PORT_BUTTONS |= ((1<<S_1)|(1<<S_2)); //activate internal pull-up resistors
	
	DDR_ENC &= ~((1<<ENC_A)|(1<<ENC_B)|(1<<S_ENC));
	PORT_ENC |= ((1<<ENC_A)|(1<<ENC_B)|(1<<S_ENC)); //activate internal pull-up resistors
	 
	//Initialize the external interrupt on for the ENC_A and the S_ENC Pins to 
	//trigger on a falling edge
	EICRA |= ((1<<ISC11)|(1<<ISC01));
	EIMSK |= ((1<<INT0)|(1<<INT1));
	
	//Initialize the pin change interrupt on the S_1 and S_2 Pin
	PCMSK0 |= ((1<<PCINT0)|(1<<PCINT2));
	PCICR |= (1<<PCIE0);

	//Initialize the Timer 1 to output a PWM Signal on the Pin
	//	that controls the set current of the E-Load
	TCCR1A |= ((1<<COM1A1)|(1<<WGM11)); //Fast PWM Mode, PWM Pin in none inverting mode
	TCCR1B |= ((1<<WGM12)|(1<<WGM13)|(1<<CS10)); //No prescaler -> fPWM = 244Hz
	CURSET_VAL = 0; //4mV offset (10mV, value needs to be 6), 1mV equals 1mA
	ICR1 = PWM_TOP; //Top-Value

	//Initialize the ADC
	ADMUX |= (1<<REFS0); //AVCC as reference voltage and right adjust the value
	ADCSRA |= (1<<ADPS1); //Prescaler division factor = 4
	
	//Activate interrupts globally
	sei();
}

int main(void)
{
	init();
	uart0_init(57600, 1, 0, 0, 0, 1);
	i2c_init(100000L);    
	ssd1306_init();
	
	ssd1306_clear();
	
	theEvent = EV_NO_EVENT;
	theState = ST_E_LOAD;	
	
    while (1) 
    {
		switch(theState)
		{
			case ST_E_LOAD:
			
				eload_runCC(eventsToDigitCtrl(&theEvent));
			
				//TODO: CC, CR and CP sub menu (with function from eload.c
			
				if (theEvent == EV_S1)
				{
					theEvent = EV_NO_EVENT;
					theState = ST_PWR_METER;
					ssd1306_clear();
					ssd1306_printf(10, 1, FONT6X8, "PWR-METER");
#ifdef _DEBUG_STATE_MACHINE_
					printf("ST_PWR_METER\n");
#endif
				}
				/*
				else if (theEvent == EV_S_ENC)
				{
					theEvent = EV_NO_EVENT;
					theState = ST_BATT;
					ssd1306_clear();
					ssd1306_printf(10, 3, FONT6X8, "BATTERY VOLTAGE:");
					ssd1306_printf(10, 5, FONT6X8, "%.2fV", (float)readBatteryVoltage()/1000);
#ifdef _DEBUG_STATE_MACHINE_
					printf("ST_BATT\n");
#endif
				}	*/
				
			break;
			case ST_PWR_METER:
				if (theEvent == EV_S1)
				{
					theEvent = EV_NO_EVENT;
					theState = ST_E_LOAD;
					ssd1306_clear();
#ifdef _DEBUG_STATE_MACHINE_
					printf("ST_E_LOAD\n");
#endif
				}
				else if (theEvent == EV_S_ENC)
				{
					theEvent = EV_NO_EVENT;
					theState = ST_BATT;
					ssd1306_clear();
					ssd1306_printf(10, 3, FONT6X8, "BATTERY VOLTAGE:");
					ssd1306_printf(25, 5, FONT6X8, "%.2fV", (float)readBatteryVoltage()/1000);
#ifdef _DEBUG_STATE_MACHINE_
					printf("ST_BATT\n");
#endif
				}
			break;
			case ST_BATT:
				if (theEvent == EV_S1)
				{
					theEvent = EV_NO_EVENT;
					theState = ST_PWR_METER;
					ssd1306_clear();
					ssd1306_printf(10, 1, FONT6X8, "PWR-METER");
#ifdef _DEBUG_STATE_MACHINE_
					printf("ST_PWR_METER\n");
#endif
				}
			break;
		}
	}
}

uint16_t readBatteryVoltage()
{
	uint16_t vBatPos/*, vBatNeg*/;
	uint16_t vBat_mV;
	
	//Enable the ADC
	ADCSRA |= (1<<ADEN);
	
	//Read the voltage of the positive battery terminal
	ADMUX &= ~(0x0F<<MUX0); //clear mux bits
	ADMUX |= (VBAT_P<<MUX0); //Select ADC channel of the positive battery voltage
	ADCSRA |= (1<<ADSC);
	while (ADCSRA & (1<<ADSC)); //what till conversation is finished
	vBatPos = ADC; // read voltage
#ifdef _DEBUG_ADC_
	printf("\nVBAT_POS: %u", vBatPos);
#endif
	
//TODO: Test if the following measurement is correct and even necessary
	/*
	//Read the voltage of the negative battery terminal
	ADMUX &= ~(0x0F<<MUX0); //clear mux bits
	ADMUX |= (VBAT_N<<MUX0); //Select ADC channel of the negative battery voltage
	ADCSRA |= (1<<ADSC);
	while (ADCSRA & (1<<ADSC)); //what till conversation is finished
	vBatNeg = ADC; // read voltage
#ifdef _DEBUG_ADC_
	printf("\nVBAT_NEG: %u", vBatNeg);
#endif
	*/
	
	//Disable the ADC
	ADCSRA &= ~(1<<ADEN);
	
	vBat_mV = (uint32_t)(vBatPos/*-vBatNeg*/)*5070/1023; //Supply voltage of the Step Up converter is 5.07V
	
	return vBat_mV;
}


//called on a falling edge on the ENC_A pin (encoder channel A)
ISR (INT0_vect)
{
	  if (PIN_ENC & (1<<ENC_B)) {
		  theEvent = EV_ENC_CW;
#ifdef _DEBUG_EVENTS_
		  printf("EV_ENC_CW\n");		  
#endif
	  }
	  else {
		  theEvent = EV_ENC_CCW;
 #ifdef _DEBUG_EVENTS_
		  printf("EV_ENC_CCW\n");
 #endif
	  }
	
}

//called on a falling edge on the S_ENC pin (when the switch of the encoder is pressed)
ISR (INT1_vect)
{
	theEvent = EV_S_ENC;
	
#ifdef _DEBUG_EVENTS_
	printf("EV_S_ENC\n");
#endif
	
	_delay_ms(100); //wait for the switch to bounce
	
	EIFR = (1<<INTF1); //clear the interrupt flag register
}

//called on a edge on the S_1 or the S_2 pin (when the Button S_1 or S_2 is pressed or released)
ISR (PCINT0_vect)
{
	static uint8_t pinButtonsOld = 0xFF;
	
	if ((pinButtonsOld ^ PIN_BUTTONS) & (1<<S_1) //if the S_1 pin has changed
		 && (PIN_BUTTONS & (1<<S_1)) == 0 )			//and has a LOW value
	{ 
		theEvent = EV_S1;
#ifdef _DEBUG_EVENTS_
		printf("EV_S1\n");
#endif
	}
	else if ((pinButtonsOld ^ PIN_BUTTONS) & (1<<S_2) //if the S_2 pin has changed
	&& (PIN_BUTTONS & (1<<S_2)) == 0)			//and has a LOW value
	{
		theEvent = EV_S2;
#ifdef _DEBUG_EVENTS_
		printf("EV_S2\n");
#endif
	}
	
	pinButtonsOld = PIN_BUTTONS;
	
	
	//Not really necessary:
	//_delay_ms(50); //wait for the switch to bounce
		
	//PCIFR = (1<<PCIF0); //clear the interrupt flag register
}