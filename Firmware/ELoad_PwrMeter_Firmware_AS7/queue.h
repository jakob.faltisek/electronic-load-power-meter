/*
 * queue.h
 *
 * Created: 30.03.2016 10:02:56
 *  Author: Jakob
 */ 


#ifndef QUEUE_H_
#define QUEUE_H_

void initTransmittQueue();
uint8_t isEmptyTransmittQueue();
uint8_t putCharInTransmittQueue(char dat);
char getCharFromTransmittQueue();
void removeAllFromTransmittQueue();

#endif /* QUEUE_H_ */