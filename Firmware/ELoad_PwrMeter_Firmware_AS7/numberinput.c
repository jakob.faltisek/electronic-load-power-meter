/*
 * numberinput.c
 *
 * Created: 05.02.2019 19:55:45
 *  Author: Jakob Faltisek
 */ 

#include <avr/io.h>
#include "numberinput.h"

uint8_t getNumOfDigits(uint16_t val)
{
	if		(val < 10)		return 1;
	else if (val < 100)		return 2;
	else if (val < 1000)	return 3;
	else if (val < 10000)	return 4;
	else					return 5;
}

enum DigitControl eventsToDigitCtrl(volatile enum _events* pTheEvent)
{
	enum DigitControl digCtrl;
	
	if (*pTheEvent == EV_ENC_CW)
	{
		*pTheEvent = EV_NO_EVENT;
		digCtrl = INC_DIGIT;
	}
	else if (*pTheEvent == EV_ENC_CCW)
	{
		*pTheEvent = EV_NO_EVENT;
		digCtrl = DEC_DIGIT;
	}
	else if (*pTheEvent == EV_S_ENC)
	{
		*pTheEvent = EV_NO_EVENT;
		digCtrl = SEL_NEXT_DIGIT;
	}
	else
	{
		digCtrl = DO_NOTHING;
	}
	
	return digCtrl;
}

void numberInput(enum DigitControl digitCtrl, uint16_t min, uint16_t max, 
					uint16_t* pValue, uint8_t* pCurPos)
{
	static uint16_t tenToThePowerOf[5] = {1, 10, 100, 1000, 10000};
	
	if (digitCtrl == SEL_NEXT_DIGIT) {
		if (++(*pCurPos) == getNumOfDigits(max))
		*pCurPos = 0; //Leave the 0-Digit out, only the 1- and the 2-Digit are active
	}

	if (digitCtrl == INC_DIGIT) {
		if (*pValue <= max - tenToThePowerOf[*pCurPos] && *pValue <= max)
			*pValue += tenToThePowerOf[*pCurPos];
		else 
			*pValue = max;
	}

	if (digitCtrl == DEC_DIGIT) {
		if (*pValue >= min + tenToThePowerOf[*pCurPos] && *pValue >= min)
			*pValue -= tenToThePowerOf[*pCurPos];
		else
			*pValue = min;
	}	
}
